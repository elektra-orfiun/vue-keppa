import Vue from 'vue'

export const state = () => ({
  selectedKep: '',
  selectedIpiresia: '',
  selectedDate: '',
  selectedSlot: '',
  storedNomoi: [],
  storedDimoi: [],
  storedIpiries: [],
  name: null,
  surname: null,
  userID: null,
  mobile: null,
  email: null
})
// ({
//   selectedKep: {
//     id: '1',
//     name: 'ΑΓΙΑΣ',
//     address: 'ΟΜΟΛΙΟ ΛΑΡΙΣΑΣ',
//     code: '456814'
//   },
//   selectedIpiresia: { id: 6, name: 'Υπηρεσία 2', time: '30' },
//   selectedDate: '2020-03-17',
//   selectedSlot: '9:00',
//   name: 'Test',
//   surname: 'test',
//   userID: 'test',
//   mobile: 'test',
//   email: 'adasd@adad.com'
// })
export const mutations = {
  addUserInfo(state, data) {
    Vue.set(state, 'name', data.name)
    Vue.set(state, 'surname', data.surname)
    Vue.set(state, 'userID', data.userID)
    Vue.set(state, 'mobile', data.mobile)
    Vue.set(state, 'email', data.email)
  },
  addSelectedKep(state, results) {
    Vue.set(state, 'selectedKep', results)
  },
  addSelectedIpiresia(state, results) {
    Vue.set(state, 'selectedIpiresia', results)
  },
  addSelectedDate(state, results) {
    Vue.set(state, 'selectedDate', results)
  },
  addSelectedSlot(state, results) {
    Vue.set(state, 'selectedSlot', results)
  }
}

export const getters = {
  getUsefull: (state) => (id) => {
    return state.usefull.find((group) => {
      return group.id === id
    })
  },
  getKeppa: (state) => {
    return state.selectedKep
  }
}
